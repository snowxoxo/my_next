import React from "react";
import Head from 'next/head'
import styles from '../styles/layout.module.css'
import Link from 'next/link'

const name = 'Salman Latheef';
export const siteTitle = 'SalmanLatheef.me';

export default function Header(props) {
    return (
        <div className="container">
            <Head>
                <title>{siteTitle} {props.title ? ':' : ''} {props.title}</title>
                <link rel="icon" href="/favicon.ico"/>
                <meta
                    name="description"
                    content="lorem ipsum"
                />
                <meta
                    property="og:image"
                    content={`https://og-image.now.sh/${encodeURI(
                        siteTitle
                    )}.png`}
                />
                <meta name="og:title" content={siteTitle}/>
                <meta name="twitter:card" content="summary_large_image"/>
            </Head>
            {props.home ? (
                <div className="profile">
                    <section className="wrapper">
                        <header className="header">
                            <img
                                src="/images/profile.png"
                                className='avatar'
                                alt={name}
                            />
                            <h1>{name}</h1>
                            <h2>Engineering Manager at Google working on <span className="highlight">Chrome</span></h2>
                        </header>
                    </section>
                </div>
            ) : (
                <div className="profile">
                    <section className="wrapper">
                        <header className="header">
                            <img
                                src="/images/profile.png"
                                className='avatar'
                                alt={name}
                            />
                            <h1>{name}</h1>
                        </header>
                    </section>
                </div>
            )}
            <main>{props.children}</main>
            {!props.home && (
                <div className={styles.backToHome}>
                    <Link href="/">
                        <a>← Back to home</a>
                    </Link>
                </div>
            )}
        </div>
    )
}
