import React from "react";
import Link from 'next/link'

export default function BlogItem(props) {
    return (
        <li key={props.blog.id}>
            <Link href={`/blog/${encodeURIComponent(props.blog.id)}`}>
                <a>
                    <div>{props.blog.title}</div>
                    <div className="date">{props.blog.datetime}</div>
                </a>
            </Link>
        </li>
    )
}

