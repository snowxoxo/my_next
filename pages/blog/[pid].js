import React from "react";
import {useRouter} from 'next/router'
import Link from 'next/link'

export default function BlogContent() {
    const router = useRouter();
    const {pid} = router.query;

    return <p>Post: {pid}</p>
}

