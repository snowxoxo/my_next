import React from "react";
import Header from '../components/Header';
import BlogItem from '../components/BlogItem';

export default function Home() {
    const title = 'Home';
    const posts = [{
        id: 1,
        title: "Visualize Data Structures in VSCode",
        datetime: "Sept 17 2020"
    }, {
        id: 2,
        title: "Fast page labelling in Chrome for Android",
        datetime: "Sept 17 2020"
    }, {
        id: 3,
        title: "Preload late-discovered Hero images faster",
        datetime: "Sept 17 2020"
    }, {
        id: 4,
        title: "Infinite Scroll without Layout Shifts",
        datetime: "Sept 17 2020"
    }];

    return (
        <Header home title={title}>
            <section className="wrapper home">
                <ul className="post-list">
                    {posts.map((item, index) => (
                        <BlogItem
                            blog={item}
                            key={index}
                            index={index}
                        />
                    ))}
                </ul>
                <nav className="post-nav" />
            </section>
        </Header>
    )
}
